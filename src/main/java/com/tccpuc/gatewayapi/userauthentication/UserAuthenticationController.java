package com.tccpuc.gatewayapi.userauthentication;

import static java.util.Optional.of;

import com.tccpuc.gatewayapi.core.security.service.AuthenticationService;
import com.tccpuc.gatewayapi.core.security.service.JwtTokenService;
import com.tccpuc.gatewayapi.core.security.service.JwtUserDetailsService;
import com.tccpuc.gatewayapi.userauthentication.dto.TokenValidationResponse;
import com.tccpuc.gatewayapi.userauthentication.dto.UserAuthenticationRequest;
import com.tccpuc.gatewayapi.userauthentication.dto.UserAuthenticationResponse;
import com.tccpuc.gatewayapi.userauthentication.exception.AuthenticationFailedException;
import com.tccpuc.gatewayapi.userauthentication.exception.TokenValidationFailedException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@CrossOrigin
@RestController
public class UserAuthenticationController {

  private final AuthenticationService authenticationService;
  private final JwtTokenService jwtTokenService;
  private final JwtUserDetailsService jwtUserDetailsService;

  @PostMapping("/users/authenticate")
  public ResponseEntity<UserAuthenticationResponse> authenticateUser(
      @RequestBody @Valid UserAuthenticationRequest authenticationRequest) throws Exception {
    authenticationService
        .authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

    return of(authenticationRequest)
        .map(UserAuthenticationRequest::getUsername)
        .map(jwtUserDetailsService::loadUserByUsername)
        .map(jwtTokenService::generateToken)
        .map(UserAuthenticationResponse::new)
        .map(ResponseEntity::ok)
        .orElseThrow(AuthenticationFailedException::new);
  }

  @GetMapping("/users/validate-token")
  public ResponseEntity<TokenValidationResponse> validateToken(
      @RequestHeader("Authorization") String authorization) {
    final String token = authorization.substring(7);

    return of(token)
        .map(jwtTokenService::getUsernameFromToken)
        .map(jwtUserDetailsService::loadUserByUsername)
        .map(userDetails -> jwtTokenService.validateToken(token, userDetails))
        .map(TokenValidationResponse::new)
        .map(ResponseEntity::ok)
        .orElseThrow(TokenValidationFailedException::new);
  }

}
