package com.tccpuc.gatewayapi.userauthentication.dto;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAuthenticationRequest {

  @NotEmpty
  private String username;

  @NotEmpty
  private String password;

}
