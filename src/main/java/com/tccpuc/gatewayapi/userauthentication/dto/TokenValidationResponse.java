package com.tccpuc.gatewayapi.userauthentication.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenValidationResponse {

  private Boolean valid;

}
