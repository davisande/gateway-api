package com.tccpuc.gatewayapi.userauthentication.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserAuthenticationResponse {

  private final String token;

}
