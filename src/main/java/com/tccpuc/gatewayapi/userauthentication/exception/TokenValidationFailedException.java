package com.tccpuc.gatewayapi.userauthentication.exception;

public class TokenValidationFailedException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Token validation error";

  public TokenValidationFailedException() {
    super(DEFAULT_MESSAGE);
  }

}
