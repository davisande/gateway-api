package com.tccpuc.gatewayapi.userauthentication.exception;

public class AuthenticationFailedException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Authentication error";

  public AuthenticationFailedException() {
    super(DEFAULT_MESSAGE);
  }

}
