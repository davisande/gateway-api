package com.tccpuc.gatewayapi.route.assetapi.response;

import lombok.Data;

@Data
public class ManufacturerResponse {

  private Integer id;
  private String name;

}
