package com.tccpuc.gatewayapi.route.assetapi;

import com.tccpuc.gatewayapi.route.assetapi.client.AssetApiClient;
import com.tccpuc.gatewayapi.route.assetapi.request.CreateAssetRequest;
import com.tccpuc.gatewayapi.route.assetapi.response.CreateAssetResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.ManufacturerResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.ProductResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.SearchAssetResponse;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AssetService {

  private final AssetApiClient assetApiClient;

  public CreateAssetResponse createAsset(@NonNull final CreateAssetRequest createAssetRequest) {
    return assetApiClient.crateAsset(createAssetRequest);
  }

  public List<SearchAssetResponse> searchAllAssets() {
    return assetApiClient.searchAllAssets();
  }

  public List<ManufacturerResponse> searchAllManufacturers() {
    return assetApiClient.searchAllManufacturers();
  }

  public List<ProductResponse> searchProductsByManufacturer(@NonNull final Integer manufacturerId) {
    return assetApiClient.searchProductsByManufacturer(manufacturerId);
  }
}
