package com.tccpuc.gatewayapi.route.assetapi.client;

import com.tccpuc.gatewayapi.route.assetapi.request.CreateAssetRequest;
import com.tccpuc.gatewayapi.route.assetapi.response.CreateAssetResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.ManufacturerResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.ProductResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.SearchAssetResponse;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "assetApi", url = "${route.assetApi.url}")
public interface AssetApiClient {

  @RequestMapping(method = RequestMethod.POST, value = "${route.assetApi.createAsset}")
  CreateAssetResponse crateAsset(@RequestBody CreateAssetRequest createAssetRequest);

  @RequestMapping(method = RequestMethod.GET, value = "${route.assetApi.createAsset}")
  List<SearchAssetResponse> searchAllAssets();

  @RequestMapping(method = RequestMethod.GET, value = "${route.assetApi.searchAllManufacturers}")
  List<ManufacturerResponse> searchAllManufacturers();

  @RequestMapping(method = RequestMethod.GET, value = "${route.assetApi.searchProductsByManufacturer}")
  List<ProductResponse> searchProductsByManufacturer(@PathVariable Integer manufacturerId);

}
