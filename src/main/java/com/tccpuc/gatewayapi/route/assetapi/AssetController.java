package com.tccpuc.gatewayapi.route.assetapi;

import com.tccpuc.gatewayapi.route.assetapi.request.CreateAssetRequest;
import com.tccpuc.gatewayapi.route.assetapi.response.CreateAssetResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.ManufacturerResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.ProductResponse;
import com.tccpuc.gatewayapi.route.assetapi.response.SearchAssetResponse;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@CrossOrigin
@RestController
public class AssetController {

  private final AssetService assetService;

  @PostMapping("assets")
  public CreateAssetResponse createAsset(
      @RequestBody @Valid final CreateAssetRequest createAssetRequest) {
    return assetService.createAsset(createAssetRequest);
  }

  @GetMapping("assets")
  public List<SearchAssetResponse> searchAllAssets() {
    return assetService.searchAllAssets();
  }

  @GetMapping("/manufacturers")
  public List<ManufacturerResponse> searchAllManufacturers() {
    return assetService.searchAllManufacturers();
  }

  @GetMapping("/manufacturers/{manufacturerId}/products")
  public List<ProductResponse> searchProductsByManufacturer(@PathVariable Integer manufacturerId) {
    return assetService.searchProductsByManufacturer(manufacturerId);
  }


}
