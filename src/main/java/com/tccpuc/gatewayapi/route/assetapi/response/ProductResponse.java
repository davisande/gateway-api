package com.tccpuc.gatewayapi.route.assetapi.response;

import lombok.Data;

@Data
public class ProductResponse {

  private Integer id;
  private String name;
  private String type;
  private ManufacturerResponse manufacturer;

}
