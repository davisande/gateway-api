package com.tccpuc.gatewayapi.route.assetapi.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.time.LocalDateTime;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class SearchAssetResponse {

  private String maintenancePeriodicity;
  private Integer amountMaintenancePeriod;
  private LocalDateTime purchaseDate;
  private LocalDateTime lastMaintenanceDate;
  private String note;
  private LocalDateTime creationDate;
  private LocalDateTime updateDate;
  private ProductResponse product;

}
