package com.tccpuc.gatewayapi.route.assetapi.request;

import java.time.LocalDateTime;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class CreateAssetRequest {

  @NotEmpty
  private String maintenancePeriodicity;

  @NonNull
  private Integer amountMaintenancePeriod;

  @NonNull
  private LocalDateTime purchaseDate;

  private LocalDateTime lastMaintenanceDate;

  private String note;

  @NonNull
  private ProductRequest product;

}
