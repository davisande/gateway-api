package com.tccpuc.gatewayapi.core.security.exception;

public class InvalidCredentialsException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Invalid credentials";

  public InvalidCredentialsException() {
    super(DEFAULT_MESSAGE);
  }

}
