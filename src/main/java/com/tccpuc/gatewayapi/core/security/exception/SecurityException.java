package com.tccpuc.gatewayapi.core.security.exception;

public class SecurityException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Security error";

  public SecurityException() {
    super(DEFAULT_MESSAGE);
  }

  public SecurityException(String message) {
    super(message);
  }
}
