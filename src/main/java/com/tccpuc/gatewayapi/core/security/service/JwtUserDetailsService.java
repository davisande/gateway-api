package com.tccpuc.gatewayapi.core.security.service;

import static java.util.Optional.of;

import java.util.ArrayList;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class JwtUserDetailsService implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(@NonNull final String username)
      throws UsernameNotFoundException {
    return of(username)
        .filter("sgauser"::equals)
        .map(user -> new User(user,
            "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
            new ArrayList<>()))
        .orElseThrow(
            () -> new UsernameNotFoundException("User not found with username: " + username));
  }
}
