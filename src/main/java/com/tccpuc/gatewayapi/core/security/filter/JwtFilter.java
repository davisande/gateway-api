package com.tccpuc.gatewayapi.core.security.filter;

import com.tccpuc.gatewayapi.core.security.service.JwtTokenService;
import com.tccpuc.gatewayapi.core.security.service.JwtUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import java.io.IOException;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtFilter extends OncePerRequestFilter {

  private final JwtTokenService jwtTokenService;
  private final JwtUserDetailsService jwtUserDetailsService;

  private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
  private static final String BEARER = "Bearer ";

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws IOException, ServletException {
    final String authRequestHeader = request.getHeader(AUTHORIZATION_HEADER_KEY);

    String username = null;
    String jwtToken = null;

    if (Objects.nonNull(authRequestHeader) && authRequestHeader.startsWith(BEARER)) {
      jwtToken = authRequestHeader.substring(7);

      try {
        username = jwtTokenService.getUsernameFromToken(jwtToken);
      } catch (IllegalArgumentException e) {
        logger.warn("Error to get token");
      } catch (ExpiredJwtException e) {
        logger.warn("Token was expired");
      }

    } else {
      logger.warn("Token doesn't has the Bearer prefix");
    }

    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (Objects.nonNull(username) && Objects.isNull(authentication)) {
      UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);

      if (jwtTokenService.validateToken(jwtToken, userDetails)) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
            new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());

        usernamePasswordAuthenticationToken
            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      }
    }

    filterChain.doFilter(request, response);
  }

}
