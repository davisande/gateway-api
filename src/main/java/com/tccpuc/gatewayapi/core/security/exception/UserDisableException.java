package com.tccpuc.gatewayapi.core.security.exception;

public class UserDisableException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "User disabled";

  public UserDisableException() {
    super(DEFAULT_MESSAGE);
  }

}
