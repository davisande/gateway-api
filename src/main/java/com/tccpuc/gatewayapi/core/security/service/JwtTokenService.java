package com.tccpuc.gatewayapi.core.security.service;

import static java.util.Optional.ofNullable;

import com.tccpuc.gatewayapi.core.security.exception.SecurityException;
import com.tccpuc.gatewayapi.core.security.property.JwtProperty;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class JwtTokenService {

  private final JwtProperty jwtProperty;

  public String generateToken(@NonNull final UserDetails userDetails) {
    return ofNullable(userDetails.getUsername())
        .map(username -> performTokenGeneration(new HashMap<>(), username))
        .orElseThrow(() -> new SecurityException("Generation token error."));
  }

  private String performTokenGeneration(final Map<String, Object> claims, final String subject) {
    return Jwts.builder()
        .setClaims(claims)
        .setSubject(subject)
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + jwtProperty.getTokenValidInMillis()))
        .signWith(SignatureAlgorithm.HS512, jwtProperty.getSecret())
        .compact();
  }

  public boolean validateToken(@NonNull final String token,
      @NonNull final UserDetails userDetails) {
    return ofNullable(getUsernameFromToken(token))
        .filter(username -> username.equals(userDetails.getUsername()) && !isTokenExpired(token))
        .isPresent();
  }

  public String getUsernameFromToken(@NonNull final String token) {
    return getClaimFromToken(token, Claims::getSubject);
  }

  public Date getExpirationDateFromToken(@NonNull final String token) {
    return getClaimFromToken(token, Claims::getExpiration);
  }

  private <T> T getClaimFromToken(final String token, final Function<Claims, T> claimsResolver) {
    return ofNullable(getAllClaimsFromToken(token))
        .map(claimsResolver)
        .orElseThrow(() -> new SecurityException("Error to get claim from token"));
  }

  private Claims getAllClaimsFromToken(final String token) {
    return Jwts.parser()
        .setSigningKey(jwtProperty.getSecret())
        .parseClaimsJws(token)
        .getBody();
  }

  private boolean isTokenExpired(final String token) {
    final Date expiration = getExpirationDateFromToken(token);

    return expiration.before(new Date());
  }

}
