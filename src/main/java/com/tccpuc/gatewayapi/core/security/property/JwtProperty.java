package com.tccpuc.gatewayapi.core.security.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("jwt")
public class JwtProperty {

  private String secret;

  private int tokenValidInMillis;

}
