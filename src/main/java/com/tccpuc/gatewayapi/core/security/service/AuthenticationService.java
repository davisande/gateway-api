package com.tccpuc.gatewayapi.core.security.service;

import com.tccpuc.gatewayapi.core.security.exception.InvalidCredentialsException;
import com.tccpuc.gatewayapi.core.security.exception.UserDisableException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AuthenticationService {

  private final AuthenticationManager authenticationManager;

  public void authenticate(@NonNull final String username, @NonNull final String password) {
    try {
      authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new UserDisableException();
    } catch (BadCredentialsException e) {
      throw new InvalidCredentialsException();
    }
  }

}
