package com.tccpuc.gatewayapi.core.advice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionAdvice {

//  @ResponseStatus(UNAUTHORIZED)
//  @ExceptionHandler({
//      UserDisableException.class,
//      InvalidCredentialsException.class
//  })
//  public Error handleSaveMonitoringEventException(final RuntimeException ex) {
//    return Error.builder().message(ex.getMessage()).build();
//  }

  @ResponseStatus(BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Errors handleMethodArgumentNotValidException(
      final MethodArgumentNotValidException ex) {
    List<String> errorMessages = new ArrayList<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      final String fieldName = ((FieldError) error).getField();
      final String errorMessage = error.getDefaultMessage();

      errorMessages.add(fieldName + ": " + errorMessage);
    });

    return Errors.builder().messages(errorMessages).build();
  }

}
